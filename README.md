**Amlet by Mopso Srl**
Amlet enhances customer due diligence for financial institutions by automating KYC processes using the Self-Sovereign Identity framework. Nowadays, onboarding is a long and complex process made of various steps of form-filling and credentials sharing: this process deeply affects customers’ experience as they are requested to perform repetitive and time-consuming tasks.

The process is not smoother nor simpler for financial institutions, as they are required to check and verify information and data with multiple third party providers, often using human-based activities and non-scalable processes

As we believe that making the KYC process smoother and more effective is a key step to improve customers’ experience, in Amlet we transform data and identity credentials into digital twins, stored into the customers' digital identity wallet. When a customer presents her own wallet, Amlet checks data using a broad range of databases to screen names, info, PEP, sanctions and terrorist lists, and Ultimate Beneficial Owners for business customers.

Thanks to the automation of onboarding and KYC process, the time required to conclude controls on users is reduced to a bunch of seconds, and anti-fraud defenses result enhanced. 

Web site: https://amlet.eu/en/
